
function add(a, b) {
    let result = a + b;
    return result;
  }
  let my_value = add(2, 3); {
    console.log(my_value);
  }
  function multiply(a, b) {
  
    // 3*4 = +3+3+3+3
    // let result = 3
    // result = result + 3
    // result = result + 3
    // result = result + 3
    // add (result, a) = soma o resultado da variável result com a variávbel a
    // loop for o primeiro parâmetro é o inicio do loop
    // o segundo parâmetro é a ccondição de execucão do loop, pois é um mecanismo de repetição
    // o terceiro parametro é o incremento do loop anterior com  o próximo
    // o  i < b-1 pois o primeiro a é declarado e o restante é loop
    let result = a
    for (let i = 0; i < b - 1; i++) {
      // tenho que somar a com a porque 2*3 é o mesmo que 2+2+2
      result = add(result, a);
    }
    return result;
  }
  let my_value_m = multiply(2, 3); {
    console.log(my_value_m);
  }
  function pow(a, b) {
    //3^4 = 3*3*3*3
    //let p = 3
    //result = 3*3
    //result = 3*3
    //result = 3*3
    let p = a
    for (let i = 0; i < b - 1; i++) {
      p = multiply(p, a);
    }
    return p;
  }
  let my_value_p = pow(2, 3);
  console.log(my_value_p);
  
  function factorial(a) {
    //3! = 3*2*1
    //let f = f
    // f = f * i ->  2 loops, pois diminui 1 loop a cada execução,
    //f = f * i -> 1 loop
    let f = a
    for (let i = 1; i < a; i++) {
      f = multiply(f, i);
    }
    return f
  }
  let my_value_f = factorial(6);
  console.log(my_value_f);
  // refazer fibonnacci
  function fibonnaci (n) {
    let a = 0, b = 1, f = b;
      for(let i = 1; i < n; i++) {
          f = add(a, b)
          a = b;
          b = f;
      }
      return f
  };
  let fibo = fibonnaci(20)
  console.log('fibo', fibo)